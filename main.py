'''
CSV Reader
functions:
    remove duplicated lines
    find target words
'''

import csv
import codecs
from chardet import detect

CSV_FILE = 'SampleCSVFile_2kb.csv'

list_csv = []
target_words = ['french', 'safco', 'storage', 'paper']


def get_file_code(csv_file) -> str:
    '''
    Extract text file decode
    '''
    try:
        with open(csv_file, 'rb') as my_file:
            rawdata = my_file.read()
    except FileNotFoundError:
        print(f"File {CSV_FILE} not found!")
        exit(1)

    try:
        codec = detect(rawdata)['encoding']
    except UnicodeDecodeError:
        print("Falha ao decodificar o tipo de padrão unicode.")

    return codec


def convert_csv_to_list(csv_file) -> list:
    '''
    Convert CSV file to Python list
    '''
    with codecs.open(csv_file, mode='r', encoding=get_file_code(csv_file)) as csv_file:
        reader = csv.reader(csv_file)
        for line in reader:
            list_csv.append(line)

    return list_csv


def find_target_word(csv_list, target):
    '''
    Search for target words
    '''
    for line in csv_list:
        for item in line:
            for word in item.split():
                if word.lower() in target:
                    print(
                        f'log: {word} found in line {csv_list.index(line) + 1}')


if __name__ == '__main__':

    find_target_word(
        convert_csv_to_list(CSV_FILE),
        target_words
    )
